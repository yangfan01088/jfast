/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.base.util;

import java.util.Map;

/**
 * 断言工具
 */
public abstract class Assert {

	/**
	 * 断言该布尔值为真
	 */
	public static void isTrue(boolean expression) {
		isTrue(expression, "[断言 错误] - 表达式必须为真");
	}

	/**
	 * 断言该布尔值为真
	 */
	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言某对象必须为null
	 */
	public static void isNull(Object object) {
		isNull(object, "[断言 错误] - 对象必须为空");
	}

	/**
	 * 断言某对象必须为null
	 */
	public static void isNull(Object object, String message) {
		if (object != null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言某对象不为null
	 */
	public static void notNull(Object object) {
		notNull(object, "[断言 错误] - 对象不可为空");
	}

	/**
	 * 断言某对象不为null
	 */
	public static void notNull(Object object, String message) {
		if (object == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言某字符串长度不为0
	 */
	public static void hasLength(String text) {
		hasLength(text, "[断言 错误] - 字符串长度必须不为0");
	}

	/**
	 * 断言某字符串长度不为0
	 */
	public static void hasLength(String text, String message) {
		if (!StringUtils.hasLength(text)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言字符串不能为空字符
	 */
	public static void hasText(String text) {
		hasText(text, "[断言 错误] - 该字符串不能为空字符");
	}

	/**
	 * 断言字符串不能为空字符
	 */
	public static void hasText(String text, String message) {
		if (!StringUtils.hasText(text)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言不包含子串
	 */
	public static void doesNotContain(String textToSearch, String substring) {
		doesNotContain(textToSearch, substring,
				"[断言 错误] - 字符串[ "+textToSearch+" ]不可包含子串 [ " + substring + " ]");
	}

	/**
	 * 断言不包含子串
	 */
	public static void doesNotContain(String textToSearch, String substring, String message) {
		if (StringUtils.hasLength(textToSearch) && StringUtils.hasLength(substring) &&
				textToSearch.contains(substring)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言对象数组不为空
	 */
	public static void notEmpty(Object[] array) {
		notEmpty(array, "[断言 错误] - 对象数组不可为空");
	}

	/**
	 * 断言对象数组不能为空
	 */
	public static void notEmpty(Object[] array, String message) {
		if (ObjectUtils.isEmpty(array)) {
			throw new IllegalArgumentException(message);
		}
	}
	
	/**
	 * 断言字符串不能为空 ，为空时抛出非法参数异常
	 */
	public static void notEmpty(String str, String message) {
		if (StringUtils.isEmpty(str)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * 断言Map不可以为空
	 */
	public static void notEmpty(Map<?, ?> map) {
		notEmpty(map, "[断言 错误] - Map不可为空");
	}

	/**
	 * 断言Map不可以为空
	 */
	public static void notEmpty(Map<?, ?> map, String message) {
		if (CollectionUtils.isEmpty(map)) {
			throw new IllegalArgumentException(message);
		}
	}

}

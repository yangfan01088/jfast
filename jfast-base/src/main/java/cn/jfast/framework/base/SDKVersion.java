package cn.jfast.framework.base;

/**
 * JFast SDK版本号声明
 */
public class SDKVersion {

    /**
     * 获取 JFast 的版本号，版本号的命名规范
     *
     * <pre>
     * [大版本号].[质量号].[发布流水号]
     * </pre>
     *
     * @return jfast.version 项目的版本号
     */
    public static String version() {
        return String.format("%d.%s.%d", majorVersion(), releaseLevel(), minorVersion());
    }

    public static int majorVersion() {
        return 1;
    }

    public static int minorVersion() {
        return 1;
    }

    public static String releaseLevel() {
        //a: 内部测试品质, b: 公测品质, r: 最终发布版
        return "1";
    }
}

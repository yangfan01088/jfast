package cn.jfast.framework.jdbc.orm.sql;

import cn.jfast.framework.jdbc.annotation.Delete;
import cn.jfast.framework.jdbc.db.ConnectionFactory;
import cn.jfast.framework.jdbc.orm.Executor;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.SQLException;

public class DeleteSql extends Executor {

    private Delete delete;

    public DeleteSql(Type[] paramTypes,
                     String[] paramNames,
                     Object[] args,
                     Delete delete,
                     Type returnType,
                     Object dao,
                     Method method) throws IllegalAccessException, SQLException, ClassNotFoundException {
        this.dao = dao;
        this.method = method;
        this.paramNames = paramNames;
        this.paramTypes = paramTypes;
        this.args = args;
        this.delete = delete;
        this.returnType = returnType;
        conn = ConnectionFactory.getThreadLocalConnection();
        super.wrapParam();
    }

    @Override
    public String getSql() {
        return delete.sql() + " ";
    }

}

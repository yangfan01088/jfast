package cn.jfast.framework.jdbc.db;

import javax.sql.DataSource;

public interface IDataSource {
	DataSource getDataSource();
}

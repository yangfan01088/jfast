package cn.jfast.framework.jdbc.db;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;

public class AlibabaDruidDataSource implements IDataSource{
	
	private DruidDataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public boolean init() {
		dataSource = new DruidDataSource();
		dataSource.setUrl(DBProperties.JDBC_URL);
		dataSource.setUsername(DBProperties.JDBC_USER);
		dataSource.setPassword(DBProperties.JDBC_PASSWORD);
		dataSource.setDriverClassName(DBProperties.JDBC_DRIVER_CLASS);
		dataSource.setMaxActive(DBProperties.MAX_POOL_SIZE);
		dataSource.setMinIdle(DBProperties.MIN_POOL_SIZE);
		dataSource.setInitialSize(DBProperties.INITIAL_POOL_SIZE);
		return true;
	}
	
	/**
	 * 关闭数据源
	 * @return
	 */
	public boolean stop() {
		if (dataSource != null)
			dataSource.close();
		return true;
	}
}

package cn.jfast.framework.jdbc.db;

public class DBProperties {

	public static String JDBC_URL = "jdbc:mysql://localhost:3306/jfast?useUnicode=true&characterEncoding=utf8";
	public static String JDBC_USER = "root";
	public static String JDBC_PASSWORD = "83884808";
	public static String JDBC_DRIVER_CLASS = "com.mysql.jdbc.Driver";
	public static int MAX_POOL_SIZE = 100;
	public static int MIN_POOL_SIZE = 10;
	public static int INITIAL_POOL_SIZE = 10;
	public static int MAX_IDLE_TIME = 20;
	public static int ACQUIRE_INCREMENT = 4;

}

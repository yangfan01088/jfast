package cn.jfast.framework.jdbc.orm;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 解析SQL可使用参数
 */
public abstract class Wrapper{

    protected Type[] paramTypes;
    protected String[] paramNames;
    protected Object[] args;
    protected Map<String,Object> paramMaps = new HashMap<String, Object>();
    protected Type returnType;
    protected Connection conn;
    protected PreparedStatement ps;
    protected List<String> sqlFields = new LinkedList<String>();
    protected Object dao;
    protected Method method;

    /**
     * 封装SQL传入参数Map
     * @return
     * @throws IllegalAccessException
     */
    public Map<String,Object> wrapParam() throws IllegalAccessException {
        String paramName = "";
        Object paramValue = "";
        for(int i=0;i<paramNames.length;i++){
            paramName = paramNames[i];
            paramValue = args[i];
            wrapPrimitiveValue(paramName,paramValue);
        }
        return  this.paramMaps;

    }

    private void wrapPrimitiveValue(String paramName,Object paramValue) throws IllegalAccessException {
        if(paramValue.getClass() == Integer.class
                ||paramValue.getClass() == Long.class
                ||paramValue.getClass() == Float.class
                ||paramValue.getClass() == Double.class
                ||paramValue.getClass() == Short.class
                ||paramValue.getClass() == Character.class
                ||paramValue.getClass() == String.class
                ||paramValue.getClass() == Character.class
                ||paramValue.getClass() == java.util.Date.class
                ||paramValue.getClass() == java.sql.Date.class
                ||paramValue.getClass() == Time.class
                ||paramValue.getClass() == Timestamp.class){
            if(null != paramMaps.get(paramName.toLowerCase()))
                throw new RuntimeException("参数列表与参数对象中有重复字段:"+paramName);
            paramMaps.put(paramName.toLowerCase(),paramValue);
        } else {
            wrapMultiValue(paramName,paramValue);
        }
    }

    private void wrapMultiValue(String paramName,Object paramValue) throws IllegalAccessException {
        Field[] fields = paramValue.getClass().getDeclaredFields();
        for(Field field:fields){
            field.setAccessible(true);
            Object obj = field.get(paramValue);
            if(obj.getClass() == Integer.class
                    ||obj.getClass() == Long.class
                    ||obj.getClass() == Float.class
                    ||obj.getClass() == Double.class
                    ||obj.getClass() == Short.class
                    ||obj.getClass() == Character.class
                    ||obj.getClass() == String.class
                    ||obj.getClass() == Character.class
                    ||obj.getClass() == java.util.Date.class
                    ||obj.getClass() == java.sql.Date.class
                    ||obj.getClass() == Time.class
                    ||obj.getClass() == Timestamp.class) {
                if(null != paramMaps.get((paramName+"."+field.getName()).toLowerCase()))
                    throw new RuntimeException("参数列表与参数对象中有重复字段:"+paramName+"."+field.getName());
                paramMaps.put((paramName+"."+field.getName()).toLowerCase(),obj);
            } else {
                wrapMultiValue(paramName+"."+field.getName(),obj);
            }
        }
    }

}

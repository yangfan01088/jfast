package cn.jfast.framework.jdbc.orm.sql;

import cn.jfast.framework.jdbc.annotation.Update;
import cn.jfast.framework.jdbc.db.ConnectionFactory;
import cn.jfast.framework.jdbc.orm.Executor;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.SQLException;

public class UpdateSql extends Executor {

    private final Update update;

    public UpdateSql(Type[] paramTypes,
                     String[] paramNames,
                     Object[] args,
                     Update update,
                     Type returnType,
                     Object dao,
                     Method method) throws IllegalAccessException, SQLException, ClassNotFoundException {
        this.dao = dao;
        this.method = method;
        this.paramNames = paramNames;
        this.paramTypes = paramTypes;
        this.args = args;
        this.update = update;
        this.returnType = returnType;
        conn = ConnectionFactory.getThreadLocalConnection();
        super.wrapParam();
    }

    @Override
    public String getSql() {
        return update.sql()+" ";
    }
}

package cn.jfast.framework.jdbc.db;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class DbcpDataSource implements IDataSource {

	private BasicDataSource dataSource;
	
	public DbcpDataSource(){
		
	}
	
	/**
	 * 数据源初始化
	 * @return
	 */
	public boolean init(){
		dataSource = new BasicDataSource();
		dataSource.setUrl(DBProperties.JDBC_URL);
		dataSource.setUsername(DBProperties.JDBC_USER);
		dataSource.setMaxIdle(DBProperties.MAX_IDLE_TIME);
		dataSource.setDriverClassName(DBProperties.JDBC_DRIVER_CLASS);
		dataSource.setPassword(DBProperties.JDBC_PASSWORD);
		dataSource.setMaxActive(DBProperties.MAX_POOL_SIZE);
		return true;
	}
	
	/**
	 * 获得数据源
	 */
	public DataSource getDataSource() {
		return dataSource;
	}
	
	/**
	 * 关闭数据源
	 * @return
	 */
	public boolean stop() {
		if (dataSource != null)
			try {
				dataSource.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return true;
	}

}

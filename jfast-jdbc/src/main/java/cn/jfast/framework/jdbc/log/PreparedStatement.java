package cn.jfast.framework.jdbc.log;

import java.lang.reflect.Proxy;

public class PreparedStatement {

	private static java.sql.PreparedStatement ps;
	private static SqlLogger handler;

	public static java.sql.PreparedStatement getPreparedStatement(java.sql.PreparedStatement ps,String sql,String targetDao) {
		handler = new SqlLogger(ps,sql,targetDao);
		PreparedStatement.ps = (java.sql.PreparedStatement) Proxy.newProxyInstance(ps.getClass()
				.getClassLoader(), new Class[]{java.sql.PreparedStatement.class}, handler);
		return PreparedStatement.ps;
	}

}

/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.log;

import java.lang.reflect.Proxy;
import cn.jfast.framework.log.jfast.JfastLogger;
import cn.jfast.framework.log.log4j.Log4jLogger;
import cn.jfast.framework.log.slf4j.Slf4jLogger;

public class LogFactory {

	public static Logger getLogger(Class<?> clazz) {
		return getLogger(LogType.AUTO, clazz);
	}

	public static Logger getLogger(LogType type, Class<?> clazz) {

		Logger log = null;

		switch (type) {
		case Slf4j:
			log = new Slf4jLogger(clazz);
			break;
		case Log4j:
			log = new Log4jLogger(clazz);
			break;
		case Jfast:
			log = new JfastLogger(clazz);
			break;
		default:
			try {
				Class.forName("org.apache.log4j.Logger");
				log = new Log4jLogger(clazz);
			} catch (Exception e) {
				try {
					Class.forName("org.slf4j.LoggerFactory");
					log = new Slf4jLogger(clazz);
				} catch (Exception ex) {
					log = new JfastLogger(clazz);
				}
			}
			break;
		}

		LoggerProxy proxy = new LoggerProxy(log);
		log =  (Logger)Proxy.newProxyInstance(log.getClass().getClassLoader(),
				log.getClass().getInterfaces(), proxy);
		return log;

	}

}

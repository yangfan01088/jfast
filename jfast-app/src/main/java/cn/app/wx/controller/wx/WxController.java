package cn.app.wx.controller.wx;

import cn.jfast.framework.plugin.wx.api.web.WeixinMutualInterface;
import cn.jfast.framework.web.api.annotation.Api;
import cn.jfast.framework.web.api.annotation.Get;
import cn.jfast.framework.web.api.annotation.Post;
import cn.jfast.framework.web.view.viewtype.Text;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api("wx")
public class WxController extends WeixinMutualInterface {

    @Get("api")
    @Override
    public Text doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        return super.doGet(req, resp);
    }

    @Post("api")
    @Override
    public Text doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        return super.doPost(req, resp);
    }

}

package cn.app.wx.dao.test;

import cn.app.wx.dao.UserDao;
import cn.app.wx.model.User;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.Logger;
import cn.jfast.framework.web.core.junit.JfastJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Resource;

@RunWith(JfastJUnit4ClassRunner.class)
public class UserTest {

    @Resource
    private UserDao userDao;

    private Logger log = LogFactory.getLogger(UserTest.class);

    @Test
    public void test(){
        User user = userDao.selectAllFromUserByEmailPassword("1297450431@qq.com","123456");
        userDao.updateUserSetPasswordByEmail("1297450431@qq.com", "123456");
        userDao.deleteFromUserByEmail("123");
        userDao.select("1297450431@qq.com").get("password");
    }

}

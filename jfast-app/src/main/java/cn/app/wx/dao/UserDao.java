package cn.app.wx.dao;

import cn.app.wx.model.User;
import cn.jfast.framework.jdbc.annotation.Body;
import cn.jfast.framework.jdbc.annotation.Dao;
import cn.jfast.framework.jdbc.annotation.Select;
import cn.jfast.framework.jdbc.db.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Think on 2015/5/28.
 */
@Dao
public class UserDao {

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    @Body
    public User selectAllFromUserByEmailPassword(String email, String password) throws SQLException, ClassNotFoundException {
        User user = new User();
        conn = ConnectionFactory.getThreadLocalConnection();
        ps = conn.prepareStatement("select * from user where email = ? and password = ?");
        ps.setString(1,email);
        ps.setString(2,password);
        rs = ps.executeQuery();
        if(rs.next()){
            user.setBirthday(rs.getDate("birthday"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setPhone(rs.getString("phone"));
            user.setSex(rs.getString("sex"));
            user.setUserid(rs.getInt("userid"));
            user.setUsername(rs.getString("username"));
            if(rs.next())
                throw new SQLException("查询目标为一条,但是查出了多条记录.");
        }
        return user;
    }

    public boolean deleteFromUserByEmail(String email){
        return false;
    }

    public int updateUserSetPasswordByEmail(String Email, String password){
        return 1;
    }

    @Select(sql = "select * from user where email = :email")
    public Map<String,Object> select(String email){
        return null;
    }

}

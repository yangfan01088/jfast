/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.api;

import cn.jfast.framework.upload.FileRequest;
import cn.jfast.framework.upload.FileRequestResolver;
import cn.jfast.framework.upload.UploadFile;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

/**
 * Api 综合请求对象
 */
public class ApiRequest {

    /** HttpServletRequest请求 */
    private HttpServletRequest request;
    /** form-data 请求 */
    private FileRequest fileRequest;
    /** 文件上传解析器 */
    private FileRequestResolver resolver;

    public ApiRequest(HttpServletRequest request) {
        this.request = request;
        resolver = new FileRequestResolver();
        try {
            fileRequest = resolver.resolveMultipart(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获得HttpServletRequest请求
     * @return
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    /**
     * 获得指定名称的上传文件
     * @param fieldName
     * @return
     */
    public UploadFile getUploadFile(String fieldName) {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getFileMap().get(fieldName);
        else
            return null;
    }

    /**
     * 获得指定名称的参数值
     * @param fieldName
     * @return
     */
    public String getParameter(String fieldName) {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getAttr(fieldName);
        else {
            return request.getParameter(fieldName);
        }
    }

    /**
     * 获得数组型参数
     * @param fieldName
     * @return
     */
    public String[] getParameters(String fieldName) {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getAttrMap().get(fieldName);
        else {
            return request.getParameterMap().get(fieldName);
        }
    }

    /**
     * 获得上传文件的Map链
     * @return
     */
    public Map<String, UploadFile> getFileMap() {
        return this.fileRequest.getFileMap();
    }

    /**
     * 获得请求域所有参数名
     * @return
     */
    public Set<String> getParameterNames() {
        if (FileRequestResolver.isMultipart(request))
            return this.fileRequest.getAttrMap().keySet();
        else
            return this.request.getParameterMap().keySet();
    }

    /**
     * 判断是否是远程服务调用请求
     * @return
     */
    public boolean isRemoteRequest() {
        boolean bool = false;
        try {
            bool = request.getInputStream().available() != 0 && !FileRequestResolver.isMultipart(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bool;
    }

    public String getMethod() {
        return this.request.getMethod();
    }

}

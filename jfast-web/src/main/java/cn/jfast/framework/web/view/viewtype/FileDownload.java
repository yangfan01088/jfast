/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.ContentType;
import cn.jfast.framework.web.view.DownloadMode;
import cn.jfast.framework.web.view.View;

public class FileDownload extends View {
    private String filename;
    private byte[] fileData;
    private DownloadMode mode;
    private ContentType contentType = ContentType.TXT;

    public FileDownload(String filename, byte[] fileData) {
        this(filename, fileData, DownloadMode.OPTION);
    }

    public FileDownload(String filename, byte[] fileData, DownloadMode mode) {
        this.fileData = fileData;
        this.filename = filename;
        this.mode = mode;
        this.contentType = getContentType(filename);
    }

    private ContentType getContentType(String filename) {
        if (filename.matches("[\\s\\S]*(.txt)"))
            return ContentType.TXT;
        else if (filename.matches("[\\s\\S]*(.doc|.docx|.docm|.dotm)"))
            return ContentType.MSWORD;
        else if (filename.matches("[\\s\\S]*(.xls|.xlsx|.xlsm|.xltx|.xltm|.xlsb)"))
            return ContentType.MSEXCEL;
        else if (filename.matches("[\\s\\S]*(.pptx|.ppt|.pptm|.ppsx|.xltx|.potx|.potm)"))
            return ContentType.MSPOWERPOINT;
        else if (filename.matches("[\\s\\S]*(.vsdx|.vsdm|.vssx|.vssm|.vstx|.vstm)"))
            return ContentType.VISIO;
        else if (filename.endsWith(".bmp"))
            return ContentType.BMP;
        else if (filename.endsWith(".gif"))
            return ContentType.GIF;
        else if (filename.endsWith(".jpeg"))
            return ContentType.JPEG;
        else if (filename.endsWith(".pdf"))
            return ContentType.PDF;
        else if (filename.endsWith(".xml"))
            return ContentType.XML;
        else if (filename.endsWith(".pdf"))
            return ContentType.PDF;
        else
            return null;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getContentType() {
        return this.contentType.getContentType();
    }

    public DownloadMode getMode() {
        return mode;
    }

    public void setMode(DownloadMode mode) {
        this.mode = mode;
    }

    @Override
    public String getView() {
        return "文件下载:" + this.getFilename();
    }

}

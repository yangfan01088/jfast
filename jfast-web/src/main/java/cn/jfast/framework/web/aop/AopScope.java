package cn.jfast.framework.web.aop;

/**
 * Created by Think on 2015/5/29.
 */
public enum AopScope {
    Global,
    Method
}

package cn.jfast.framework.web.view.viewtype;

import cn.jfast.framework.web.view.View;

/**
 * Created by Think on 2015/5/28.
 */
public class Text extends View {

    private String view;

    public Text(String view) {
        this.view = view;
    }

    @Override
    public String getView() {
        return this.view;
    }

    @Override
    public String toString() {
        return "普通文本:" + view;
    }
}

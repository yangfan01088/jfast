package cn.jfast.framework.web.aop;

import cn.jfast.framework.jdbc.db.ConnectionFactory;
import cn.jfast.framework.web.aop.annotation.Aop;
import cn.jfast.framework.web.api.ApiInvocation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

/**
 * 事务管理拦截器
 */
@Aop(scope = AopScope.Method)
public class TransactionAopHandler extends AopHandler {

    private Connection conn;

    @Override
    public void beforeHandle(ApiInvocation invoke, HttpServletRequest request, HttpServletResponse response, List<Exception> ex) throws Exception {
        conn = ConnectionFactory.getThreadLocalConnection();
        if (null != conn && !conn.isClosed() && conn.getAutoCommit() == true)
            conn.setAutoCommit(false);
        invoke.invoke();

    }

    @Override
    public void afterHandle(ApiInvocation invoke, HttpServletRequest request, HttpServletResponse response, List<Exception> ex) throws Exception {
        if (null != conn && !conn.isClosed() && conn.getAutoCommit() == false) {
            if (null == ex || ex.isEmpty()) {
                conn.commit();
            } else {
                conn.rollback();
            }
            conn.setAutoCommit(true);
            conn.close();
        }
        invoke.invoke();
    }

}
